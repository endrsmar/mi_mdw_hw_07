/*
 * Created by Martin Endrst as part of MI-MDW class at CTU FIT
 */
package core;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Martin Endrst
 */
public class Booking implements Serializable {
    
    public int id;
    public String firstName;
    public String lastName;
    public String departureAirport;
    public Date departureDate;
    public String arrivalAirport;
    public Date arrivalDate;
    
    public Booking() { }
    public Booking(String firstName,
                   String lastName,
                   String departureAirport,
                   Date departureDate,
                   String arrivalAirport,
                   Date arrivalDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.departureAirport = departureAirport;
        this.departureDate = departureDate;
        this.arrivalAirport = arrivalAirport;
        this.arrivalDate = arrivalDate;
    }
    
}

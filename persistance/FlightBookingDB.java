/*
 * Created by Martin Endrst as part of MI-MDW course at CTU FIT
 */
package persistance;

import core.Booking;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Martin Endrst
 */
public class FlightBookingDB {
    private static final List<Booking> database = new ArrayList<Booking>();
    private static int lastId = 0;
    
    public static List<Booking> findAll(){
        return database;
    }
    
    public static Booking find(int id){
        for (Booking booking : database){
            if (booking.id == id){ return booking; }
        }
        return null;
    }
    
    public static void persist(Booking booking){
        booking.id = ++lastId;
        database.add(booking);
    }
    
    public static void update(Booking booking){
        database.remove(find(booking.id));
        database.add(booking);
    }
    
    public static void remove(int id){
        Booking booking = find(id);
        if (booking == null) return;
        database.remove(booking);
    }
}

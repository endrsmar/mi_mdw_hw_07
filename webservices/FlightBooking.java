/*
 * Created by Martin Endrst as part of MI-MDW course at CTU FIT
 */
package webservices;

import core.Booking;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import persistance.FlightBookingDB;

/**
 *
 * @author Martin Endrst
 */
@WebService(serviceName = "FlightBooking")
public class FlightBooking {

    @WebMethod(operationName = "list")
    @WebResult(name="booking")
    public List<Booking> list() {
        return FlightBookingDB.findAll();
    }

    @WebMethod(operationName = "create")
    @WebResult(name="booking")
    public Booking create(@WebParam(name = "booking") Booking booking) {
        FlightBookingDB.persist(booking);
        return booking;
    }
    
    @WebMethod(operationName = "update")
    @WebResult(name="booking")
    public Booking update(@WebParam(name = "booking") Booking booking) {
        FlightBookingDB.update(booking);
        return booking;
    }
    
    @WebMethod(operationName = "remove")
    public void remove(@WebParam(name = "id") int id) {
        FlightBookingDB.remove(id);
    }
}
